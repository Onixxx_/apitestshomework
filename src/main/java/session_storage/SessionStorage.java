package session_storage;

import java.util.HashMap;
import java.util.Map;

public class SessionStorage {

    private Map<String, Object> storage;

    public SessionStorage() {
        storage = new HashMap<>();
    }

    public void addProperty(String key, Object value) {
        storage.put(key, value);
    }

    public Object getProperty(String key) {
        return storage.get(key);
    }
}
