package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class CartPage {

    @FindBy(css = "div.product-summary__row")
    List<WebElement> products;

    public CartPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    // знаю, не самое красивое решение, но лучше придумать не хватило сил и времени:(
    public boolean containsExpectedProduct(String productCode, Integer quantity) {
        for (WebElement product: products) {
            if (product.findElement(By.cssSelector("a.product-summary__img-link"))
                    .getAttribute("href").endsWith(productCode)) {
                if (product.findElement(By.cssSelector("e2-quantity-select.qty-select span.select__selected-option"))
                .getText().equals(quantity.toString())) {
                    return true;
                }
            }
        }

        return false;
    }
}
