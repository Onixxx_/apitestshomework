package payload;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Product {

    private String code;

    private Product() {

    }

    public String getCode() {
        return code;
    }

    public static Builder newBuilder() {
        return new Product().new Builder();
    }

    public class Builder {

        private Builder() {

        }

        public Builder setCode(String code) {
            Product.this.code = code;
            return this;
        }

        public Product build() {
            return Product.this;
        }
    }
}
