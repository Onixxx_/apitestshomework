package payload;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Cart {

    private Product product;

    private int quantity;

    private Cart() {

    }

    public Product getProduct() {
        return product;
    }

    public int getQuantity() {
        return quantity;
    }

    public static Builder newBuilder() {
        return new Cart().new Builder();
    }

    public class Builder {

        private Builder() {

        }

        public Builder setProduct(Product product) {
            Cart.this.product = product;
            return this;
        }

        public Builder setQuantity(int quantity) {
            Cart.this.quantity = quantity;
            return this;
        }

        public Cart build() {
            return Cart.this;
        }
    }
}
