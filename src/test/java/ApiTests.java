import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.commons.io.FileUtils;
import org.apache.commons.text.StringSubstitutor;
import org.everit.json.schema.Schema;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.html5.LocalStorage;
import org.openqa.selenium.html5.WebStorage;
import pages.CartPage;
import payload.Cart;
import payload.Product;
import session_storage.SessionStorage;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static io.restassured.RestAssured.given;
import static java.time.LocalTime.now;
import static org.junit.Assert.assertTrue;

public class ApiTests {

    RequestSpecification requestSpec;
    SessionStorage storage;
    WebDriver driver;

    private final String PRODUCT_CODE = "2876350";
    private final int PRODUCT_QUANTITY = 1;

    @Before
    public void setUp() {
        requestSpec = new RequestSpecBuilder()
                .setBaseUri("https://www.kruidvat.nl")
                .setAccept(ContentType.JSON)
                .setContentType(ContentType.JSON)
                .build();

        storage = new SessionStorage();
    }

    @Test
    public void apiTestUseModelBuilder() {
        createCart();
        addProductPayloadUseModelBuilder();
        List<String> response = getCartResponseCodes();
        assertTrue(response.contains(PRODUCT_CODE));
    }

    @Test
    public void apiTestUseTemplate() {
        createCart();
        addProductPayloadUseTemplate();
        List<String> response = getCartResponseCodes();
        assertTrue(response.contains(PRODUCT_CODE));
    }

    @Test
    public void apiTestUseMapToPojo() {
        createCart();
        addProductPayloadUseMapToPojo();
        List<String> response = getCartResponseCodes();
        assertTrue(response.contains(PRODUCT_CODE));
    }

    @Test
    public void verifyJSONSchema() {
        createCart();
        addProductPayloadUseMapToPojo();
        Response response = given().spec(requestSpec)
                .when().get("/api/v2/kvn/users/anonymous/carts/{guid}", storage.getProperty("guid"));
        JSONObject jsonObject = new JSONObject(response.getBody().asString());
        JSONObject jsonSchema = new JSONObject(
                new JSONTokener(ApiTests.class.getResourceAsStream("json_schema.json")));
        Schema schema = SchemaLoader.load(jsonSchema);
        schema.validate(jsonObject);
    }

    @Test
    public void apiWithUiTest() {
        setUpDriver();
        createCart();
        addProductPayloadUseModelBuilder();
        setCookiesAndLocalStorage();
        driver.navigate().refresh();
        driver.get("https://www.kruidvat.nl/cart");

        CartPage cartPage = new CartPage(driver);
        assertTrue(cartPage.containsExpectedProduct(PRODUCT_CODE, PRODUCT_QUANTITY));
        driver.close();
    }


    private void createCart() {
        Response response = given()
                .spec(requestSpec)
                .when().post("/api/v2/kvn/users/anonymous/carts")
                .then().statusCode(201)
                .extract().response();
        JSONObject jsonObject = new JSONObject(response.getBody().asString());
        storage.addProperty("guid", jsonObject.getString("guid"));
    }

    private void addProductToCart(Object payload) {
         given().spec(requestSpec).and()
                .body(payload)
                .when().post("/api/v2/kvn/users/anonymous/carts/{guid}/entries", storage.getProperty("guid"))
                .then().statusCode(200);
    }

    private void addProductPayloadUseModelBuilder() {
        Product product = Product.newBuilder()
                .setCode(PRODUCT_CODE)
                .build();
        Cart cart = Cart.newBuilder()
                .setProduct(product)
                .setQuantity(PRODUCT_QUANTITY)
                .build();

        addProductToCart(cart);
    }

    private void addProductPayloadUseTemplate() {
        Map<String, Object> values = new HashMap<>();
        values.put("code", PRODUCT_CODE);
        values.put("quantity", PRODUCT_QUANTITY);

        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("product_payload.txt").getFile());
        try {
            String template = FileUtils.readFileToString(file, "UTF-8");
            StringSubstitutor substitutor = new StringSubstitutor(values, "{{", "}}");
            String cartPayload = substitutor.replace(template).replaceAll("\\s", "");
            addProductToCart(cartPayload);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void addProductPayloadUseMapToPojo() {
        Map<String, Object> values = new HashMap<>();
        values.put("code", PRODUCT_CODE);
        values.put("quantity", PRODUCT_QUANTITY);

        ObjectMapper objectMapper = new ObjectMapper();
        Product product = objectMapper.convertValue(values, Product.class);
        values.put("product", product);
        Cart cart = objectMapper.convertValue(values, Cart.class);

        addProductToCart(cart);
    }

    private List<String> getCartResponseCodes() {
        return given().spec(requestSpec)
                .when().get("/api/v2/kvn/users/anonymous/carts/{guid}", storage.getProperty("guid"))
                .jsonPath().getList("entries.product.code");
    }

    private void setUpDriver() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        driver.get("https://www.kruidvat.nl");
    }

    private void setCookiesAndLocalStorage() {
        driver.manage().deleteAllCookies();
        driver.manage().addCookie(new Cookie("kvn-cart", storage.getProperty("guid").toString()));
        driver.manage().addCookie(new Cookie("OptanonAlertBoxClosed", now().toString()));

        LocalStorage local = ((WebStorage) driver).getLocalStorage();
        local.setItem("country", "NL");
    }
}
